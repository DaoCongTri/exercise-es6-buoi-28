export class Food {
  constructor(name, type, discount, img, desc, price, status, id) {
    this.id = id;
    this.name = name;
    this.type = type;
    this.price = price;
    this.discount = discount;
    this.status = status;
    this.img = img;
    this.desc = desc;
  }
  totalDiscount() {
    return this.price - (this.price * this.discount) / 100;
  }
}
