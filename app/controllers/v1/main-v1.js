import showThongTinLenForm, { layThongTinTuForm } from "./controller-v1.js";
import { Food } from "../../models/v1/model.js";
export function themMon() {
  let data = layThongTinTuForm();
  console.log(data);
  let { name, type, discount, img, desc, price, status, id } = data;
  let food = new Food(name, type, discount, img, desc, price, status, id);
  showThongTinLenForm(food);
}
window.themMon = themMon;
