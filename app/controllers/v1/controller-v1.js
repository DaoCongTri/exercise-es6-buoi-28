export function layThongTinTuForm() {
  let id = document.getElementById("foodID").value;
  let name = document.getElementById("tenMon").value;
  let type = document.getElementById("loai").value;
  let price = document.getElementById("giaMon").value * 1;
  let discount = document.getElementById("khuyenMai").value * 1;
  let status = document.getElementById("tinhTrang").value;
  let img = document.getElementById("hinhMon").value;
  let desc = document.getElementById("moTa").value;
  return {
    name,
    type,
    discount,
    img,
    desc,
    price,
    status,
    id,
  };
}
export default function showThongTinLenForm(food) {
  let { name, type, discount, img, desc, price, status, id } = food;
  document.getElementById("spMa").innerHTML = id;
  document.getElementById("spTenMon").innerHTML = name;
  document.getElementById("spLoaiMon").innerHTML =
    type == "loai1" ? "Chay" : "Mặn";
  document.getElementById("spGia").innerHTML = price;
  document.getElementById("spKM").innerHTML = discount;
  document.getElementById("spTT").innerHTML = status == "0" ? "Hết" : "Còn";
  document.getElementById("imgMonAn").src = img;
  document.getElementById("pMoTa").innerHTML = desc;
  document.getElementById("spGiaKM").innerHTML = price * discount;
}
