export let renderFoodList = (foodArr) => {
  var contentHTML = "";
  foodArr.forEach((item) => {
    console.log(item);
    let { name, type, discount, img, desc, price, status, id } = item;
    var contentTR = `
            <tr>
                <td>${id}</td>
                <td>${name}</td>
                <td>
                  <img src="${img}" alt="" width="100%" heigth="100%">
                </td>
                <td>${type ? "Chay" : "Mặn"}</td>
                <td>${price}</td>
                <td>${discount}</td>
                <td>${item.totalDiscount()}</td>
                <td>${status ? "Còn" : "Hết"}</td>
                <td>${desc}</td>
                <td>
                  <button type="button" class="btn btn-danger" onclick="xoaMon(${id})">Delete</button>
                  <button type="button" class="btn btn-success" data-toggle="modal"
                data-target="#exampleModal" onclick="editMon(${id})">Edit</button>
                </td>
            </tr>
        `;
    contentHTML += contentTR;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};
let colors = ["red", "blue", "yellow", "green"];
colors.forEach((item) => {
  console.log(item);
});
let newColor = colors.map((item) => {
  return item.toUpperCase();
});
console.log(newColor);
export function showThongTinLenForm2(food) {
  let { name, type, discount, img, desc, price, status, id } = food;
  document.getElementById("foodID").value = id;
  document.getElementById("tenMon").value = name;
  document.getElementById("loai").value = type ? "loai1" : "loai2";
  document.getElementById("giaMon").value = price;
  document.getElementById("khuyenMai").value = discount;
  document.getElementById("tinhTrang").value = status ? "1" : "0";
  document.getElementById("hinhMon").value = img;
  document.getElementById("moTa").value = desc;
}
window.showThongTinLenForm2 = showThongTinLenForm2;
