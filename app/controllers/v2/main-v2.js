import { renderFoodList } from "./controller-v2.js";
import { Food } from "../../models/v1/model.js";
import { layThongTinTuForm } from "../v1/controller-v1.js";
import { showThongTinLenForm2 } from "./controller-v2.js";
const BASE_URL = "https://64413df4792fe886a8a23776.mockapi.io/food";
let idSelected = 0;
let fetchFoodList = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      console.log(res.data);
      let foodArr = res.data.map((item) => {
        let { name, type, discount, img, desc, price, status, id } = item;
        return new Food(name, type, discount, img, desc, price, status, id);
      });
      renderFoodList(foodArr);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchFoodList();
let themMon = () => {
  axios({
    url: BASE_URL,
    method: "POST",
    data: layThongTinTuForm(),
  })
    .then((res) => {
      fetchFoodList();
      console.log(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.themMon = themMon;
let xoaMon = (id) => {
  console.log(id);
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.xoaMon = xoaMon;
let editMon = (id) => {
  idSelected = id;
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log("res data: ", res);
      showThongTinLenForm2(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};
window.editMon = editMon;
window.updateMon = () => {
  axios({
    url: `${BASE_URL}/${idSelected}`,
    method: "PUT",
    data: layThongTinTuForm(),
  })
    .then((res) => {
      console.log(res.data);
      fetchFoodList();
    })
    .catch((err) => {
      console.log(err);
    });
};
